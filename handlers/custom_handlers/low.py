from telebot.types import Message
from config_data import config
import os
import requests
from bs4 import BeautifulSoup

from loader import bot


# Low  хендлер, поиск по низкой цене
@bot.message_handler(commands=["low"])
def bot_low(message: Message):
    product = bot.send_message(
        message.chat.id, 'Укажите название товара'
    )
    bot.register_next_step_handler(product, choose_low_price)

def choose_low_price(product):
    print(os.getenv("RAPID_API_KEY"))
    prd = str(product.text)
    search_api = f"https://www.citilink.ru/search/?text={prd}"#str(os.getenv("RAPID_API_KEY"))
    print(search_api)
    search_api += str(product.text)
    print(search_api)
    request = requests.get(search_api)
    bs = BeautifulSoup(request.text, "html.parser")
    print(bs)
    all_links = bs.find_all("a", class_="app-catalog-9gnskf e1259i3g0")
    for link in all_links:
        print(link["href"])
